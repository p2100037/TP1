#include "liste.hpp"

#include <iostream>
#include <cassert>

Liste::Liste() : adPremiere(nullptr) {
  /* votre code ici */

}

Liste::Liste(const Liste& autre) : adPremiere(nullptr) {
  /* votre code ici */
  const Cellule* c = autre.tete();
  while(c != nullptr){
    ajouter_en_queue(c->valeur);
    c = c->suivant;
  }
}

Liste& Liste::operator=(const Liste& autre) {
  // Vérifier l'auto-attribution
  if (this != &autre) {
    // Libérer la mémoire de la liste existante
    while (adPremiere != nullptr) {
      Cellule* temp = adPremiere;
      adPremiere = adPremiere->suivant;
      delete temp;
    }

    // Copier les éléments de la liste "autre"
    const Cellule* c = autre.tete();
    while (c != nullptr) {
      ajouter_en_queue(c->valeur);
      c = c->suivant;
    }
  }

  return *this;
}

Liste::~Liste() {
  /* votre code ici */
  while (adPremiere != nullptr) {
    Cellule* temp = adPremiere;
    adPremiere = adPremiere->suivant;
    delete temp;
  }
}

void Liste::ajouter_en_tete(int valeur) {
  /* votre code ici */
  Cellule* c = new Cellule();
  c->valeur = valeur;
  c->suivant = adPremiere;
  adPremiere = c;
}

void Liste::ajouter_en_queue(int valeur) {
  /* votre code ici */
  Cellule* c = new Cellule();
  c->valeur = valeur;
  if(adPremiere != nullptr){
    queue()->suivant = c;
  }
  else{
    ajouter_en_tete(valeur);
  }
}

void Liste::supprimer_en_tete() {
  /* votre code ici */
  const Cellule* c = adPremiere;
  adPremiere = adPremiere->suivant;
  delete c;
}

Cellule* Liste::tete() {
  /* votre code ici */
  return adPremiere;
}

const Cellule* Liste::tete() const {
  /* votre code ici */
  return adPremiere ;
}

Cellule* Liste::queue() {
  /* votre code ici */
  Cellule* c;

  c = adPremiere;

  if(c == nullptr){
    return nullptr;
  }

  while(c->suivant != nullptr){
    c = c-> suivant;
  }
  return c ;
}

const Cellule* Liste::queue() const {
  /* votre code ici */
  const Cellule* c;

  c = adPremiere;

  if(c == nullptr){
    return nullptr;
  }

  while(c->suivant != nullptr){
    c = c-> suivant;
  }
  return c ;
}

int Liste::taille() const {
  /* votre code ici */
  Cellule* c = adPremiere;
  int nb = 0;
  while(c != nullptr){
    c = c->suivant;
    nb++;
  }
  return nb ;
}

Cellule* Liste::recherche(int valeur) {
  /* votre code ici */
  Cellule* c;
  c = adPremiere;
  while(c != nullptr){
    if(c->valeur == valeur){
      return c;
    }
    c = c->suivant;
  }
  return nullptr ;
}

const Cellule* Liste::recherche(int valeur) const {
  /* votre code ici */
  const Cellule* c;
  c = adPremiere;
  while(c != nullptr){
    if(c->valeur == valeur){
      return c;
    }
    c = c->suivant;
  }
  return nullptr ;
}

void Liste::afficher() const {
  Cellule* c = adPremiere;
  /* votre code ici */
  cout << "[ " ;
  while(c != nullptr){
  cout << c->valeur << " ";
  c = c->suivant;
  }
  cout << "]" << endl;
}
 